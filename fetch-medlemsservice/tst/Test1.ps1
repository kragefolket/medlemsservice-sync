param(
    [String]$MedlemUsername = $env:MEDLEM_LOGIN_USERNAME,
    [String]$MedlemPassword = $env:MEDLEM_LOGIN_PASSWORD
)

$ErrorActionPreference = "STOP"
Import-Module (Join-Path $PSScriptRoot "../src/Medlemsservice" -Resolve) -RequiredVersion 1.0.0 -Force
#Set-MedlemsserviceProxy -Uri "http://127.0.0.1:8888"
Invoke-MedlemsserviceLogin -Username $MedlemUsername -Password $MedlemPassword

$units = Get-MedlemsserviceStructure
$group = $units | Where-object { $_.organization_type_id -eq 2 } | Select-Object -first 1
$group | Format-List

$units | Where-object { $_.organization_type_id -eq 1 } | ForEach-Object {
    $unit = $_
    Write-Host ("Unit {0} ({1})" -f $unit.display_name, $unit.id)
    $result = Get-MedlemsserviceMemberList -GroupId $group.id -StructureId $unit.id
    Write-host ("{2} members in Unit {0} ({1})" -f $unit.display_name, $unit.id, $result.Length)
}
 
$all = Get-MedlemsserviceMemberList -GroupId $group.id
Write-Host ("All members: {0}" -f $all.Length)
