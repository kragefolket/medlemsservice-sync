param(
    [String]$GroupName = $env:GROUPNAME,
    [String]$MedlemUsername = $env:MEDLEM_LOGIN_USERNAME,
    [String]$MedlemPassword = $env:MEDLEM_LOGIN_PASSWORD
)

$ErrorActionPreference = "STOP"
Set-MedlemsserviceProxy "http://127.0.0.1:8888"
Import-Module (Join-Path $PSScriptRoot "../src/Medlemsservice" -Resolve) -RequiredVersion 1.0.0 -Force
Invoke-MedlemsserviceLogin -Username $MedlemUsername -Password $MedlemPassword

# Get-MedlemsserviceModelFields "member.organization"
# Get-MedlemsserviceModelFields "member.profile" 
# Get-MedlemsserviceModelFields "member.function"
# Get-MedlemsserviceModelFields "res.partner.relation.all"
#return


$units = Get-MedlemsserviceStructure
$group = $units | Where-object { $_.organization_type_id -eq 2 } | Select-Object -first 1

#$all = Get-MedlemsserviceMemberList -OrgId $group.id | ForEach-Object { Get-MedlemsserviceMemberDetails -MemberId $_.id -GroupId $group.id }
#$all | Format-List

#$krit = Get-MedlemsserviceMemberDetails -GroupId $group.id -MemberId 193592
#$krit.Details | Format-List
#$krit.Functions | Format-Table
#$krit.Relations | Format-List
#
#$kieranId = $krit.Relations | Where-Object { $_.Type -eq "Mor til"} | Select-Object -ExpandProperty MemberId
#$kieran = Get-MedlemsserviceMemberDetails -GroupId $group.id -MemberId $kieranId
#$kieran.Details | Format-List
#$kieran.Relations | Format-Table
#$kieran.Functions | Format-Table

#$jb = Get-MedlemsserviceMemberDetails -GroupId $group.id -MemberNo 16040047 -ExpandRelations
#$jb.Details | Format-List
#$jb.Relations | Format-List
##$lukas = Get-MedlemsserviceMemberList -Groupid $group.id -MemberNo $jb.Relations[0].MemberNo
#$lukas = $jb.Relations | Select-object -First 1
#$lukas | Format-List

Get-MedlemsserviceMemberDetails -GroupId $group.Id -MemberNo 16040034 | ConvertTo-Json -Depth 10