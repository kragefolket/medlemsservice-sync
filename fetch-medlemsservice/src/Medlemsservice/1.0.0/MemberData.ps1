$global:medlemsserviceUnits = $Null
$global:medlemsserviceUnitIds = $Null

$memberDefaultFields = @(
    "name",
    "member_number",
    "active_function_ids",
    "partner_id",
    "relation_all_ids",
    "email",
    "first_name",
    "last_name",
    "phone",
    "mobile",
    "permission_photo",
    "complete_address"
)

function Get-MedlemsserviceStructure {
    # Get-MedlemsserviceModelFields -Model "member.organization"
    $result = Read-MedlemsserviceDataset -Model "member.organization" -Fields @("display_name", "organization_type_id")
    $result.records
}

function Get-MedlemsserviceMemberList {
    param(
        [Parameter(Mandatory = $true)]
        $GroupId,
        [Parameter(Mandatory = $false)]
        $StructureId = $Null,
        [Parameter(Mandatory = $false)]
        $Fields = $memberDefaultFields,
        [Parameter(Mandatory = $false)]
        [String]$MemberNo = $null,
        [Switch]$AlsoNonActive
    )

    $criteria = @(,
        @("organization_id", "=", $GroupId)
    )
    if ("" -ne "$StructureId") {
        $criteria += @(,
            @("member_id.function_ids.organization_id", "child_of", $StructureId)
        )
    }
    If(-not $AlsoNonActive) {
        $criteria += @(,
            @("state", "=", "active")
        )
    }


    if ("" -ne "$MemberNo") {
        $criteria += @(,
            @("member_number", "=", $MemberNo)
        )
    }

    Read-MedlemsserviceDataset -Model "member.profile" -Fields $Fields -Params @{
        domain = $criteria
    } | Select-Object -ExpandProperty records
}

function Get-MedlemsserviceMember {
    param(
        [Parameter(Mandatory = $true)]
        $MemberId,
        [Parameter(Mandatory = $false)]
        $Fields = $memberDefaultFields,
        $LimitProfile = $Null
    )

    $params = @{
        args   = @(
            @(, $MemberId),
            $Fields
        )
        kwargs = @{
            context = @{
                bin_size = $true
            }
        }
        method = "read"
        model  = "member.profile"
    }

    New-MedlemsserviceCallRequest -Path "/web/dataset/call_kw/member.profile/read" -Params $params -ContextParameterName "kwargs"
}

function Get-MedlemsserviceFunction {
    param(
        $MemberId,
        $FunctionId,
        $Fields = @(
            "id",
            "active",
            "top_info",
            "organization_id",
            "function_type_id",
            "func_org_name",
            "display_name",
            "leader_function"
        ),
        [Switch]$SkipDetails
    )

    $params = @{
        method = "read"
        model  = "member.function"
        args   = @(
            @(, $FunctionId),
            $Fields
        )
        kwargs = @{
            context = @{
                bin_size      = $True
                limit_profile = $MemberId
            }
        }
    }

    $result = New-MedlemsserviceCallRequest -Path "/web/dataset/call_kw/member.function/read" -Params $params -ContextParameterName "kwargs"
    $result | ForEach-Object {
        $details = $_
        if($SkipDetails) {
            $details = $Null
        }
        [PSCustomObject]@{
            Unit = $_.display_name
            Function = $_.function_type_id[1]
            IsLeader = $_.leader_function
            OrgId = $_.organization_id[0]
            Details = $details
        }
    }
}

function Get-MedlemsserviceMemberIdForRelation {
    param(
        $GroupId,
        $MemberId,
        $PartnerId,
        $RecordId,
        $Description
    )

    $params = @{
        args       = @(
            @(, $RecordId),
            ${
                active_id=$PartnerId
                active_ids=@(,$PartnerId)
                active_modes="res.partner"
                active_org_id=$GroupId
                default_left_partner_id=$PartnerId
                default_left_profile_id=$MemberId
                default_this_partner_id=$PartnerId
                limit_profile=$MemberId
                search_default_this_partner_id=$PartnerId

                tz = $global:MedlemsserviceContext.user_context.tz
                lang = $global:MedlemsserviceContext.user_context.lang
                uid = $global:MedlemsserviceContext.user_context.uid
            }
        )
        context_id = 1
        method     = "action_open_profile"
        model      = "res.partner.relation.all"
    }

    try {
        $result = New-MedlemsserviceCallRequest -Path "/web/dataset/call_button" -Params $params -SkipContext
        $result.res_id
    }
    catch {
        Write-Warning "Cannot open $Description"
        $Null
    }
}

function Get-MedlemsserviceRelation {
    param(
        $GroupId,
        $MemberId,
        $PartnerId,
        $RelationId,
        $Fields = @(
            "this_partner_id",
            "contact_type",
            "type_selection_id",
            "other_partner_id",
            "relation_id",
            "this_primary_contact"
        ),
        [Switch]$Expand,
        [Switch]$SkipExpandFunctionDetails
    )

    $params = @{
        method = "read"
        model  = "res.partner.relation.all"
        args   = @(
            @(, $RelationId),
            $Fields
        )
        kwargs = @{
            context = @{
                active_id                      = $PartnerId
                active_ids                     = @(, $PartnerId)
                active_model                   = "res.partner"
                active_org_id                  = $GroupId
                bin_size                       = $True
                default_left_partner_id        = $PartnerId
                default_left_profile_id        = $MemberId
                default_this_partner_id        = $PartnerId
                limit_profile                  = $MemberId
                search_default_this_partner_id = $PartnerId
            }
        }
    }

    $relations = New-MedlemsserviceCallRequest -Path "/web/dataset/call_kw/res.partner.relation.all/read" -Params $params -ContextParameterName "kwargs"
    $results = @()
    $relations | ForEach-Object {
        $type = $_.type_selection_id[1]
        $parts = $_.other_partner_id[1].Split(" ", 2)
        $memberNo = $parts[0]
        $memberName = $parts[1]

        if ($Expand) {
            try {
                $details = Get-MedlemsserviceMemberDetails -MemberNo $memberNo -GroupId $GroupId -SkipFunctionDetails:$SkipExpandFunctionDetails
            } catch {
                Write-Warning ("Error while expanding $memberName ($memberNo) $_")
                $details = $Null
            }

            $results += [PSCustomObject]@{
                Type            = $type
                MemberNo        = $memberNo
                MemberName      = $memberName
                MemberDetails   = $details
            }
        }
        else {
            $results += [PSCustomObject]@{
                Type       = $type
                MemberNo   = $memberNo
                MemberName = $memberName
            }
        }
    }
    $results
}

function Get-MedlemsserviceMemberDetails {
    param(
        [Parameter(Mandatory = $true)]
        [string]$MemberNo,
        [Parameter(Mandatory = $true)]
        $GroupId,
        $Fields = $memberDefaultFields,
        [Switch]$ExpandRelations,
        [Switch]$SkipFunctionDetails
    )

    if ($null -eq $global:medlemsserviceUnits) {
        $global:medlemsserviceUnits = Get-MedlemsserviceStructure
        $global:medlemsserviceUnitIds = $medlemsserviceUnits | Select-object -ExpandProperty id
    }
    
    $member = Get-MedlemsserviceMemberList -MemberNo $MemberNo -GroupId $GroupId -Fields $Fields -AlsoNonActive
    if($Null -eq $member) {
        Write-Warning "Could not get member $MemberNo"
        Return
    }
    $relations = $member.relation_all_ids | ForEach-Object {
        $relationId = $_
        Get-MedlemsserviceRelation -GroupId $GroupId `
            -MemberId $member.id -PartnerId $member.partner_id[0] `
            -RelationId $relationId `
            -Expand:$ExpandRelations `
            -SkipExpandFunctionDetails:$SkipFunctionDetails
    }
    $functionIds = @()
    $functionIds += TryGetMember -InputObject $member -Property active_function_ids
    $functions = $functionIds | ForEach-Object {
        Get-MedlemsserviceFunction -MemberId $member.id -FunctionId $_ -SkipDetails:$SkipFunctionDetails
    }

    [PSCustomObject]@{
        Details   = $member
        Relations = $relations
        Functions = $functions | Where-Object {
            $orgId = TryGetMember -InputObject $_ -Property OrgId
            return $Null -eq $orgId -or $global:medlemsserviceUnitIds.Contains($orgId[0]) }
    }
}
