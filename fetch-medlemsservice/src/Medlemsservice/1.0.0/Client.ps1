$global:MedlemsserviceUrl = "https://medlemsservice.spejdernet.dk"
$global:MedlemsserviceSession = $Null
$global:MedlemsserviceContext = $Null

$defaultProperties = @{
    Proxy       = $Null
    ContentType = "application/json"
    Headers     = @{
        "X-Requested-With" = "XMLHttpRequest"
    }
}

function Set-MedlemsserviceUrl {
    [CmdletBinding()]
    param($ServerUrl)

    $global:MedlemsserviceUrl = $ServerUrl
}

function Set-MedlemsserviceProxy {
    param(
        [Parameter(Mandatory=$True, Position=0)]
        $Uri
    )

    $defaultProperties.Proxy = $Uri
}

function Invoke-MedlemsserviceLogin {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('AvoidUsingPlainTextForPassword', 'Password')]
    param(
        $Username,
        $Password
    )

    If("$Username" -eq "" -or "$Password" -eq "") {
        throw "Username and password must be specified"
    }

    Invoke-WebRequest -SessionVariable "SessionVariable" -uri "${global:MedlemsserviceUrl}/web/login" -Method GET -Proxy $defaultProperties.Proxy | Out-Null
    $global:MedlemsserviceSession = $SessionVariable
    Invoke-WebRequest -WebSession $global:MedlemsserviceSession -uri "${global:MedlemsserviceUrl}/web/login" -Method POST -ContentType "application/x-www-form-urlencoded" -Body "login=${Username}&password=${Password}" -Proxy $defaultProperties.Proxy | Out-Null

    $global:MedlemsserviceContext = New-MedlemsserviceCallRequest -Path "/web/session/get_session_info"
}

function TryGetMember {
    param(
        [Parameter(Mandatory=$True, Position=0)]
        $InputObject,
        [Parameter(Mandatory=$True, Position=1)]
        $PropertyName
    )

    try {
        $InputObject.$propertyName
    } catch {
        $Null
    }
}

function New-MedlemsserviceCallRequest {
    param(
        $Path,
        $Params = @{},
        $ContextParameterName = $Null,
        [Switch]$SkipContext
    )

    $req = @{
        jsonrpc = "2.0"
        method  = "call"
        params  = $Params
    }

    If (-not $SkipContext -and $Null -ne $global:MedlemsserviceContext) {
        $value = $req.params
        if ($Null -ne $ContextParameterName) { $value = $value.$ContextParameterName }
        If ($Null -eq ( TryGetMember -InputObject $value -PropertyName "context")) { $value.context = @{} }
        $value = $value.context
        $value.tz = $global:MedlemsserviceContext.user_context.tz
        $value.lang = $global:MedlemsserviceContext.user_context.lang
        $value.uid = $global:MedlemsserviceContext.user_context.uid
    }

    $body = $req | ConvertTo-Json -Depth 10
    $result = Invoke-RestMethod -WebSession $global:MedlemsserviceSession `
        -Uri "${global:MedlemsserviceUrl}${Path}" `
        -Method POST -Body $body `
        @defaultProperties

    if ($result.PSObject.Properties.Name -contains "error") {
        Write-Error $result.error
    }
    else {
        $result | Select-Object -ExpandProperty result
    }
}
