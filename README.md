# Medlemsservice synkronisering

Dette projekt bruges til at hente medlemmer fra Medlemsservice
og opdatere relaterede systemer.

Dette er flyttet til 
* https://dev.azure.com/kragefolket/medlemsservice-sync
* https://dev.azure.com/kragefolket/medlemsservice-jobs
