param(
    $ContactsFile = (Join-Path $PWD "./out/Contacts.json" -Resolve),
    $GroupsFile = (Join-Path $PWD "./out/Groups.json" -Resolve)
)

$contacts = Get-Content -Path $ContactsFile | ConvertFrom-Json
$groups = Get-Content -Path $GroupsFile | ConvertFrom-Json

$groups | ForEach-Object {
    $group = $_
    $groupName = $group.Group
    $memberIds = $group.Members | Select-Object -ExpandProperty link
    $members   = $contacts | Where-Object { $memberIds -contains $_.link }
    $phones = $members | Select-Object -ExpandProperty mobil 
    Write-Host ""
    Write-Host "Mobil numre for ${groupName}"
    Write-Host ($phones -join ";")
}