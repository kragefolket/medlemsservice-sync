param(
    [ValidateScript({Test-Path $_})]
    $DataFile = (Join-Path $PWD "out/data.json" -Resolve),
    [ValidateScript({Test-Path $_})]
    $GroupsFile = (Join-Path $PSScriptRoot "Group-Definitions.json" -Resolve),
    $OutputContactsFile = (Join-Path $PWD "out/Contacts.json"),
    $OutputGroupsFile = (Join-Path $PWD "out/Groups.json"),
    $OutputDuplicatesFile = (Join-Path $PWD "out/Duplicates.json"),
    $GroupName = "Hellerup Gruppe", $env:GROUPNAME
)

$ErrorActionPreference = "STOP"

$global:relativekey = "Relation"
$data = ConvertFrom-Json (Get-Content $DataFile -Raw -Encoding UTF8)
$groups = ConvertFrom-Json (Get-Content $GroupsFile -Raw -Encoding UTF8)
$global:units = $data.units | Select-Object -ExpandProperty name

$global:members = @()

function TryGetMember {
    param(
        $InputObject,
        $Property
    )
    try {
        $InputObject.$Property
    } catch {
        $Null
    }
}

function ProcessMember {
    param(
        $memberitm
    )

    $link = "/member/mypage/{0}" -f $memberitm.Details.member_number
    $existingItems = $global:members | Where-Object { $_.link -eq $link }
    $current = $existingItems | Select-Object -First 1
    $exists = $null -ne $current -and $existingItems.Length -eq 1
    # If($_.name -like "Jesper Ulrik Balle") {
    #     "Write-Host $_.name $exists"
    # }

    $relatives = @()
    $relatives += SlimlineRelatives -relatives $memberitm.Relations
    $assignments = @()
    $assignments += $memberitm.Functions | Select-Object -property `
            @{name = 'unit'; expression = { $_.Unit } },
            @{name = 'assignment'; expression = { $_.Function } }

    If (-not $exists) {
        $memberobj = [PSCustomObject]@{
            name        = $memberitm.Details.name
            member_number = $memberitm.Details.member_number
            link        = $link
            email       = $memberitm.Details.email
            mobil       = $memberitm.Details.mobile
            telefon     = $memberitm.Details.phone
            assignments = $assignments
            relatives   = $relatives
            title       = ""
        }
        if($memberobj.mobil -eq $false) { $memberobj.mobil = $Null}
        if($memberobj.email -eq $false) { $memberobj.email = $Null}
        if($memberobj.telefon -eq $false) { $memberobj.telefon = $Null}
        $global:members += $memberobj
    }
    Else {    
        $current.assignments += Compare-Object $current.assignments $assignments -Property unit, assignment | Where-Object { $_.SideIndicator -eq "=>" }
        $current.relatives += Compare-Object $current.relatives $relatives -Property relation, link | Where-Object { $_.SideIndicator -eq "=>" }
    }
}

function ProcessRelatives {
    param(
        $memberitm
    )

    $relations = TryGetMember -InputObject $memberitm -property Relations
    if($Null -eq $relations) {
        Write-Verbose ("Skipping undefined relatives for {0} {1}" -f $memberitm.name, ($memberitm| ConvertTo-Json))
        Return
    } else {
        $relations | ForEach-Object {
            ProcessRelative -memberitm $memberitm -itm $_.MemberDetails
        }
    }
}

function ProcessRelative {
    param(
        $memberitm,
        $itm
    )

    $link = "/member/mypage/{0}" -f $itm.Details.member_number
    $assignments = @()
    $assignments += $itm.Functions | Select-Object -property `
        @{name = 'unit'; expression = { $_.Unit } },
        @{name = 'assignment'; expression = { $_.Function } } `
        | Where-Object { ("{0}" -f $_.unit) -ne "" -and ("{0}" -f $_.assignment) -ne "" }
    $memberitm.Functions | Where-Object { $_.Function -eq "Enhedsmedlem" } | ForEach-Object {
        $assignments += [PSCustomObject]@{
            unit       = $_.Unit
            assignment = $global:relativekey
        }
    }

    $current = $global:members | Where-Object { $_.link -eq $link } | Select-Object -First 1
    $exists = $Null -ne $current

    $relatives = @()
    $relatives += SlimlineRelatives -relatives $itm.Relations

    If (-not $exists) {
        $memberobj = [PSCustomObject]@{
            name        = $itm.Details.name
            member_number = $itm.Details.member_number
            link        = $link
            assignments = $assignments
            email       = $itm.Details.email
            relatives   = $relatives
            mobil       = $itm.Details.mobile
            telefon     = $itm.Details.phone
            title       = ""
        }
        if($memberobj.mobil -eq $false) { $memberobj.mobil = $Null}
        if($memberobj.email -eq $false) { $memberobj.email = $Null}
        if($memberobj.telefon -eq $false) { $memberobj.telefon = $Null}

        $global:members += $memberobj
    }
    Else {    
        $current.assignments += Compare-Object $current.assignments $assignments -Property unit, assignment | Where-Object { $_.SideIndicator -eq "=>" }
        $current.relatives += Compare-Object $current.relatives $relatives -Property relation, link | Where-Object { $_.SideIndicator -eq "=>" }
    }
}

function SlimlineRelatives {
    param($relatives) 

    $result = @()
    $relatives | ForEach-Object {
        $link = "/member/mypage/{0}" -f $_.MemberNo
        $result += [PSCustomObject]@{
            relation = $_.Type
            link     = $link
            name     = $_.MemberName
        }
    }

    return $result
}

# Flatten
Write-Host ("Flatten {0} items in data file" -f $data.members.Count)
$data.members | ForEach-Object { 
    ProcessMember -memberitm $_
    ProcessRelatives -memberitm $_
} 
Write-Host ("Total items in flattened list is {0} contacts" -f $global:members.Count)

# Unify relative links with name and relation
$global:members | ForEach-Object {
    $relatives = @()
    $_.relatives | ForEach-Object {
        $link = $_.link
        $name = $global:members | Where-Object { $_.link -eq $link } | Select-Object -First 1 -ExpandProperty name
        if ($Null -eq $name) {
            $name = $_.name
        }
        $relativeUnit = (($global:members | Where-Object { $_.link -eq $link } | Select-Object -First 1).assignments `
            | Where-Object { $_.assignment -ne $global:relativekey } `
            | Select-Object -ExpandProperty unit) -join ", "

        $relatives += [PSCustomObject]@{
            name     = $name
            link     = $link
            relation = $_.relation
            unit     = $relativeUnit
        }
    }
    $_.relatives = $relatives

    $title = ""
    $title += $_.assignments | Where-Object { $_.unit -eq $GroupName -and $_.assignment -like "Gruppe*" } | Select-Object -First 1 -ExpandProperty "assignment"
    if ($title -eq "") {
        $title += $_.assignments | Where-Object { $_.assignment -eq "Enhedsmedlem" } | Select-Object -First 1 -ExpandProperty "unit"
    }

    if ($title -ne "" -and $title -ne "Andre" -and $title -notlike "Gruppe*" ) {
        $_.title = $title.trim('e')
    }
    elseif ($relatives.Length -gt 0) {
        $relationTitle = $relatives | Select-Object -First 1 -ExpandProperty "relation"
        $names = ($relatives | ForEach-Object { "{0} {1}" -f $_.unit.Trim('e'), $_.name }) -join " og "
        $relTitle = "{0} {1}" -f $relationTitle, $names.Trim()
        if ($title -ne "") {
            $_.title = ("{0} og {1}" -f $title, $relTitle)
        }
        else {
            $_.title = $relTitle
        }
    }
    elseif ($title -eq "") {
        $titles = @()
        $_.assignments | ForEach-Object { $titles += "{0} hos {1}" -f $_.assignment, $_.unit }
        $title = $titles -join " og "
        $_.title = $title
    }
    else {
        $_.title = $title
    }
}


# Special actions
## Fjern Elisabeth, samme mail som Bjørn...
$global:members = $global:members | Where-Object { $_.link -ne "/member/mypage/16040010" }

# Find duplicate e-mails
$duplicates = $global:members | Group-Object -Property email | Where-Object { $_.Count -gt 1 -and $_.Name -ne "" }
If ($duplicates.Count -gt 0) {
    Write-Host "Duplicates:"
    $duplicates | Select-Object -ExpandProperty Group | Select-Object -Property email, name, link | Format-Table
    ConvertTo-Json $duplicates -Depth 7 | Set-Content $OutputDuplicatesFile
    $duplicates | ForEach-Object {
        $ids = $_.Group | Select-Object -ExpandProperty link
        $idToKeep = $ids | Select-Object -First 1
        $idsToRemove = @()
        $idsToRemove += $ids | Where-Object { $_ -ne $idToKeep }
        $assignments = $global:members | Where-Object { ($idsToRemove.Contains($_.link)) } | Select-Object -ExpandProperty assignments
        $global:members = $global:members | Where-Object { -not ($idsToRemove.Contains($_.link)) }
        $itmToKeep = ($global:members | Where-Object { $_.link -eq $idToKeep } | Select-object -First 1)
        $itmToKeep.assignments += $assignments
    }
}


# Build groups

function FindMembersMatching {
    param(
        $conditions
    )

    $groupMembers = @()
    $global:members | Where-Object { $_.email -ne "" } | ForEach-Object {
        $MemberData = $_
        $fulfillConditions = @()
        $fulfillConditions += FindMatchingAssignmentsForGroupItem -conditions $conditions -MemberData $_

        if ($fulfillConditions.Length -gt 0) {
            $groupMemberItm = [PSCustomObject]@{
                name    = $MemberData.name
                link    = $MemberData.link
                email   = $MemberData.email
                mobil   = $MemberData.mobil
                telefon = $MemberData.telefon
                matches = $fulfillConditions
            }
            $groupMembers += @(, $groupMemberItm)
        }
    }

    return ($groupMembers | Sort-Object -unique link)
}

function FindMatchingAssignmentsForGroupItem {
    param(
        $conditions,
        $MemberData
    )

    $fulfillConditions = @()

    $conditions | ForEach-Object {
        $condition = $_
        $MemberData.assignments | ForEach-Object {
            $assignment = $_
            $specified = ($Null -ne $condition.unit) -or ($Null -ne $condition.func)
            $unitMatch = ($Null -eq $condition.unit) -or ($condition.unit -eq $assignment.unit)
            $funcMatch = ($Null -eq $condition.func) -or ($condition.func -eq $assignment.assignment)


            if ($unitMatch -eq $True -and $funcMatch -eq $True -and $specified -eq $True) {
                $fulfillConditions += $assignment
            }
        }
    }

    $memberLevelConditions = @()
    $conditions | ForEach-Object { 
        if($Null -ne $_.member_number) {
            $memberLevelConditions += $_
        }
    }

    $memberLevelConditions | ForEach-Object {
        $condition = $_
        $MemberData | Where-Object { 
            $member = $_
            $member_number_match = $member.member_number -eq $condition.member_number
            if($member_number_match) {
                $fulfillConditions += $condition
            }
        }
    }

    return $fulfillConditions
}

function BuildMailGroup {
    param(
        $Group,
        $conditions,
        $groupMembers = @()
    )
    $groupMembers += FindMembersMatching -conditions $conditions

    return [PSCustomObject]@{
        Group   = $_.Group
        Name    = $_.Name
        Members = @() + $groupMembers
    }
}

Write-Host "Processing groups..."
$result = @()
$groups | ForEach-Object {
    $groupName = $_.Group
    $itm = BuildMailGroup -Group $_ -conditions $_.Filter -groupMembers $_.Members
    $result += $itm
    Write-Host ("Found {0:00} contacts for group {1}" -f $itm.Members.Count, $groupName)
}

$result | ConvertTo-Json -Depth 5 | Set-Content -Path $OutputGroupsFile -Encoding UTF8
Write-Host ("Wrote {0} groups to {1}" -f $result.Count, $OutputGroupsFile)

$addedContacts = @()
$groups | ForEach-Object { 
    if ( $_.Members.Length -gt 0) { 
        $addedContacts += $_.Members
    } 
}
$contacts = $global:members + $addedContacts
$contacts | ConvertTo-Json -Depth 7 | Set-Content -Path $OutputContactsFile -Encoding UTF8
Write-Host ("Wrote {0} contacts to {1}" -f $contacts.Count, $OutputContactsFile)

Write-Host "Done" -ForegroundColor Green
