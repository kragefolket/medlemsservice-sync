param(
    $ContactsFile = (Join-Path $PWD "./out/Contacts.json")
)

$ErrorActionPreference = "STOP"

$contactsData = ConvertFrom-Json (Get-Content $ContactsFile -Raw -Encoding UTF8)
$contactsData | ForEach-Object {
    if ($_.email -ne "") {
        $current = $_
        $others = $contactsData | Where-Object { $_.link -ne $current.link -and $_.email -eq $current.email }
        if($others.Count -ne 0) {
            Write-Host ("Same e-mail for {0} and {1} ({2})" -f $current.link, (($others | Select-Object -ExpandProperty link) -join " and "), $_.email)
        }
    }
}