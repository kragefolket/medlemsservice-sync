param(
    [Switch]$SkipPull,
    [Switch]$SkipFetch,
    [Switch]$SkipTransform,
    [Switch]$SkipWrite,

    [ValidateNotNullOrEmpty()]
    [String]$GroupName = $env:GROUPNAME,
    [ValidateNotNullOrEmpty()] 
    [String]$MedlemUsername = $env:MEDLEM_LOGIN_USERNAME,
    [ValidateNotNullOrEmpty()]
    [String]$MedlemPassword = $env:MEDLEM_LOGIN_PASSWORD,

    [String]$OfficeUsername = $env:OFFICE365_USERNAME, 
    [SecureString]$OfficePassword = (ConvertTo-SecureString $env:OFFICE365_PASSWORD -AsPlainText -Force),

    $GroupsFile = (Join-Path $PSScriptRoot "transform/Group-Definitions.json" -Resolve),

    [Switch]$WhatIf
)

$ErrorActionPreference = "STOP"

Set-Location $PSScriptRoot

If($WhatIf) {
    Write-Host "WhatIf is enabled"
}


If ($SkipFetch) {
    Write-Host "Skipping fetch part"
}
else {
    ./fetch-medlemsservice/FetchAll.ps1 -GroupName $GroupName -MedlemUsername $MedlemUsername -MedlemPassword $MedlemPassword
}

If ($SkipTransform) {
    Write-Host "Skipping transform part"
}
else {
    ./transform/Transform-Data.ps1 -GroupsFile $GroupsFile -GroupName $GroupName
    ./transform/Print-Mobiles.ps1
}

If ($SkipWrite) {
    Write-Host "Skipping write part"
}
else {
    ./write-office365/Run.ps1 -Username $OfficeUsername -Password $OfficePassword -WhatIf:$WhatIf
}
