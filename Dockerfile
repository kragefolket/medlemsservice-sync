ARG BASE_IMAGE=mcr.microsoft.com/powershell:7.0.3-ubuntu-18.04
FROM ${BASE_IMAGE}

# RUN pwsh -Command \
#     Register-PackageSource -Trusted -ProviderName 'PowerShellGet' -Name 'Posh Test Gallery' -Location https://www.poshtestgallery.com/api/v2/ ; \
#     Install-Module -Name AzureAD.Standard.Preview

WORKDIR /app
ADD . .
RUN chmod a+x *.ps1 \
    && chmod a+x ./fetch-medlemsservice/*.ps1 \
    && chmod a+x ./transform/*.ps1 \
    && chmod a+x ./write-office365/*.ps1

#ENTRYPOINT pwsh /app/run-entrypoint.ps1