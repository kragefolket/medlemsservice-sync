param(
    $GroupsFile = (Join-Path $PWD "out/Groups.json" -Resolve),
    [Switch]$WhatIf
)

$groupsData = ConvertFrom-Json (Get-Content $GroupsFile -Raw -Encoding UTF8)

function SetGroupMembers {
    param(
        $DistributionGroup,
        $MemberEmails
    )
    
    If ($MemberEmails -eq $Null) { $MemberEmails = @() }

    $existingMembers = @()
    $existingMembers += Get-DistributionGroupMember -Identity $DistributionGroup.Identity | Select-Object -ExpandProperty PrimarySmtpAddress
    Compare-Object $existingMembers $MemberEmails | ForEach-Object {
        $member = $_.InputObject
        $action = "none"
        try {
            if ($_.SideIndicator -eq "<=") {
                $action = "remove"
                If ($WhatIf) {
                    Write-Host ("Remove-DistributionGroupMember {0} from {1}" -f $member, $DistributionGroup.Identity)
                }
                else {
                    Remove-DistributionGroupMember -Identity $DistributionGroup.Identity -Member $member -Confirm:$false
                }
            }
            elseif ($_.SideIndicator -eq "=>") {
                $action = "add"
                If ($WhatIf) {
                    Write-Host ("Add-DistributionGroupMember {0} to {1}" -f $member, $DistributionGroup.Identity)
                }
                else {
                    Add-DistributionGroupMember -Identity $DistributionGroup.Identity -Member $member
                }
            }
        }
        catch {
            Write-Warning ("Could not {0} {1}: {2}" -f $action, $member, $_)
        }
    }
}


Get-DistributionGroup | ForEach-Object {
    $distGroup = $_
    $groupData = $groupsData | Where-Object { $_.Group -eq $distGroup.PrimarySmtpAddress } | Select-Object -First 1
    If ($Null -ne $groupData) {
        Write-Host ("Updating {0}" -f $groupData.Group)
        $members = $groupData.Members | Select-Object -ExpandProperty email
        SetGroupMembers -DistributionGroup $distGroup -MemberEmails $members

        if(-not $WhatIf) {
            Write-Host ("Members of {0} group are now:" -f $groupData.Group)
            Get-DistributionGroupMember -Identity $distGroup.Identity | Format-Table -Property Name, PrimarySmtpAddress, Title
        }
    }
}

Write-Host ""
Write-Host "Done!" -ForegroundColor Green
