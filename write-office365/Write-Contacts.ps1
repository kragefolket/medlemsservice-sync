param(
    $ContactsFile = (Join-Path $PWD "out/Contacts.json" -Resolve),
    $DiffByIdFile = (Join-Path $PWD "out/Diff-ById.json"),
    $DiffByMailFile = (Join-Path $PWD "out/Diff-ByMail.json"),
    $ExistingContactsFile = (Join-Path $PWD "out/Existing.json"),
    $GroupName = $env:GROUPNAME,
    [Switch]$WhatIf
)

$ErrorActionPreference = "STOP"

# $a.PSObject.TypeNames -contains $PsTypeGuestUser
$PsTypeGuestUser = "Deserialized.Microsoft.Exchange.Data.Directory.Management.MailUser"
$PsTypeContact = "Deserialized.Microsoft.Exchange.Data.Directory.Management.MailContact"

$contactsData = ConvertFrom-Json (Get-Content $ContactsFile -Raw -Encoding UTF8)
$newContacts = @()
$contactsData | Where-Object { "" -ne ("{0}" -f $_.email) } | ForEach-Object {
    $id = $_.link
    if ($null -eq $id) { $id = $_.email }
    if ($null -ne $id) {
        $newContacts += [PSCustomObject]@{
            Name               = $_.name
            PrimarySmtpAddress = $_.email
            CustomAttribute1   = $_.link
            Title              = $_.title
            Details            = $_
        }
    }
}
$existingContacts = @()
$existingContacts += Get-MailContact -ResultSize 1000
$existingContacts += Get-MailUser -Filter "(RecipientType -eq 'MailUser' -and RecipientTypeDetailsValue -eq 'GuestMailUser')" -ResultSize 1000

$compareResult = @()
$compareResult += Compare-Object $existingContacts $newContacts -Property CustomAttribute1
$compareResult | ConvertTo-Json | Set-Content -Path $DiffByIdFile
Compare-Object $existingContacts $newContacts -Property PrimarySmtpAddress, CustomAttribute1 | ConvertTo-Json | Set-Content -Path $DiffByMailFile
$existingContacts | ConvertTo-Json  | Set-Content -Path $ExistingContactsFile
$contactsToRemove = $compareResult | Where-Object { $_.SideIndicator -eq "<=" -and $_.CustomAttribute1 -ne "" }
$contactsToAdd = $compareResult | Where-Object { $_.SideIndicator -eq "=>" }
$contactsToUpdate = @()
$newContacts | Where-Object { 
    $id = $_.CustomAttribute1
    $addItm = $contactsToAdd | Where-Object { $_.CustomAttribute1 -eq $id } | Select-object -First 1
    If ($Null -eq $addItm) {
        $contactsToUpdate += $_
    }
}

Write-Host ("Will update contacts: addding {0}, updating {1}, removing {2}" -f $contactsToAdd.Count, $contactsToUpdate.Count, $contactsToRemove.Count)

$contactsToRemove | ForEach-Object {
    $id = $_.CustomAttribute1
    $itm = $existingContacts | Where-Object { $_.CustomAttribute1 -eq $id } | Select-Object -First 1
    If ($WhatIf) {
        Write-Host ("Would remove contact {0} {1}" -f $id, $itm.Name)
    }
    else {
        Write-Host ("Removing contact {0} {1}" -f $id, $itm.Name)
        try {
            $itm | Remove-MailContact -Confirm:$false
        }
        catch {
            Write-Warning ("Could not remove {0}: {1}" -f $itm.Name, $_)
        }
    }
}

function UpdateContact {
    param(
        $itm,
        $mailContact
    )

    if ($WhatIf) {
        throw "UpdateContact called with -WhatIf enabled"
    }

    If ($mailContact.PSObject.TypeNames -contains $PsTypeContact) {
        UpdateMailContact -mailContact $mailContact -itm $itm
    }
    ElseIf ($mailContact.PSObject.TypeNames -contains $PsTypeGuestUser) {
        try {
            UpdateGuestUser -mailContact $mailContact -itm $itm
        } catch {
            Write-Host ("Error while updating guest user {0}" -f $_)
        }
    } Else {
        $mailContact.PSObject.TypeNames | format-table
        Write-Warning "Unsupported type to update"
    }
}

function UpdateGuestUser {
    param(
        $itm,
        $user
    )

    if ($WhatIf) {
        throw "UpdateContact called with -WhatIf enabled"
    }

    If (-not ($mailContact.PSObject.TypeNames -contains $PsTypeGuestUser)) {
        $mailContact.PSObject.TypeNames | format-table
        $types = $mailContact.PSObject.TypeNames -join " and "
        throw "UpdateMailContact called with incompatible object (${types})"
    }

    if($user.HiddenFromAddressListsEnabled) {
        Write-Host "Set hidden from address list"
        $user | Set-MailUser -HiddenFromAddressListsEnabled $false
    }

    If ($itm.Name -ne $user.Name) {
        Write-Host "Set name"
        $user | Set-MailUser -Name $itm.Name
    }

    If($itm.Id -ne $user.CustomAttribute1) {
        Write-Host "Set CustomAttribute1"
        $user | Set-Mailuser -CustomAttribute1 $itm.Id
    }
}

function UpdateMailContact {
    param(
        $itm,
        $mailContact
    )

    if ($WhatIf) {
        throw "UpdateContact called with -WhatIf enabled"
    }

    If (-not ($mailContact.PSObject.TypeNames -contains $PsTypeContact)) {
        $mailContact.PSObject.TypeNames | format-table
        $types = $mailContact.PSObject.TypeNames -join " and "
        throw "UpdateMailContact called with incompatible object (${types})"
    }

    $mobil = $itm.Details.mobil
    $phone = $itm.Details.telefon

    $contact = $mailContact | Get-Contact
    $existingId = $mailContact.CustomAttribute1
    $existingTitle = $contact.Title
    $existingMobile = $contact.MobilePhone
    $existingPhone = $contact.Phone
    $existingMail = $mailContact.PrimarySmtpAddress

    try {
        if ("${existingId}" -eq "") {
            Write-Host "Update CustomAttribute1"
            $mailContact | Set-MailContact -CustomAttribute1 $itm.CustomAttribute1
        }

        If ($itm.Name -ne $mailContact.Name) {
            Write-Host "Update Name"
            $mailContact | Set-MailContact -Name $itm.Name
        }

        If ($itm.Title -ne $existingTitle) {
            Write-Host "Update Title"
            $contact | Set-Contact -Title $itm.Title -Company $GroupName
        }

        If ($itm.PrimarySmtpAddress -ne "" -and $itm.PrimarySmtpAddress -ne $existingMail) {
            Write-Host "Update ExternalEmailAddress"
            $contact | Set-MailContact -ExternalEmailAddress $itm.PrimarySmtpAddress
        }

        if ("${existingMobile}" -ne "${mobil}") {
            Write-Host "Update MobilePhone"
            $contact | Set-Contact -MobilePhone "${mobil}"
        }

        if ("${existingPhone}" -ne "${phone}") {
            Write-Host "Update Phone from ${existingPhone} to ${phone}"
            $contact | Set-Contact -Phone "${phone}"
        }
    }
    catch {
        Write-Warning ("Could not update {0}: {1}" -f $name, $_)
    }
}

$contactsToAdd | ForEach-Object {
    try {
        $id = $_.CustomAttribute1
        $itm = $newContacts | Where-Object { $_.CustomAttribute1 -eq $id }
        If ($WhatIf) {
            Write-Host ("Would add contact {0} {1}" -f $id, $itm.Name)
        }
        else {
            Write-Host ("Adding contact {0} {1}" -f $id, $itm.Name)
            $contact = New-MailContact -Name $itm.Name -ExternalEmailAddress $itm.PrimarySmtpAddress
            UpdateContact -itm $itm -mailContact $contact
        }
    }
    catch {
        Write-Warning ("Could not create {0}: {1}" -f $itm.Name, $_)
    }
}

$contactsToUpdate | ForEach-Object {
    $id = $_.CustomAttribute1
    $name = $_.Name
    $contact = $existingContacts | Where-Object { $_.CustomAttribute1 -eq $id }

    If ($contact -eq $Null) {
        Write-Warning ("Should update contact {0} {1} but could not be found" -f $id, $name)
    }
    Elseif(-not $WhatIf -or $id -contains "16040047") {
        Write-Host ("Updating contact {0} {1}" -f $id, $name)
        UpdateContact -itm $_ -mailContact $contact
    }
    ElseIf ($WhatIf) {
        Write-Host ("Would update contact {0} {1}" -f $id, $name)
    }
}
