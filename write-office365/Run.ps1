param(
    [String]$Username = $env:OFFICE365_USERNAME, 
    [SecureString]$Password = (ConvertTo-SecureString $env:OFFICE365_PASSWORD -AsPlainText -Force),
    [Switch]$SkipConnect,
    [Switch]$SkipDisconnect,
    [Switch]$SkipContacts,
    [Switch]$SkipGroups,
    $ContactsFile = (Join-Path $PWD "./out/Contacts.json" -Resolve),
    $GroupsFile = (Join-Path $PWD "./out/Groups.json" -Resolve),
    [Switch]$WhatIf,
    $ConfigurationName = "Microsoft.Exchange",
    $ConnectionUri = "https://outlook.office365.com/powershell-liveid/"
)

$ErrorActionPreference = "STOP"
$ProgressPreference = "SilentlyContinue"

If(-not $SkipConnect) {
    if ("$Username" -eq "") { throw "Username needs to be specified" }
    if ("$Password" -eq "") { throw "Password needs to be specified" }

    $creds = [PSCredential]::New($Username, $Password)
    Write-Output "Creating session..."
    $Session = New-PSSession -ConfigurationName $ConfigurationName -ConnectionUri $ConnectionUri -Credential $creds -Authentication Basic -AllowRedirection
    Write-Output "Importing session..."
    Import-PsSession $Session -DisableNameChecking -AllowClobber | Out-Null
    Write-Host "Connected" -ForegroundColor Green
}

If($WhatIf) {
    Write-Host "Running with -WhatIf switch enabled"
}

If(-not $SkipContacts) {
    & $PSScriptRoot/Write-Contacts.ps1 -ContactsFile $ContactsFile -WhatIf:$WhatIf
}

If(-not $SkipGroups) {
    & $PSScriptRoot/Write-Groups.ps1 -GroupsFile $GroupsFile -WhatIf:$WhatIf
}

If(-not $SkipDisconnect) {
    Write-Host "Remove sesssion"
    Remove-PSSession $Session
}
